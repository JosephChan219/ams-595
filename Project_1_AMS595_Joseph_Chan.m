% Joseph Chan
% AMS 595
% Project #1
% Monte Carlo Simulation to Estimate pi 
% Code will automaticaly run part 1, after part 1 is finish, Matlab will
% prompt user to input an integer for part 2 to begin. 

clear % clear the workspace
clc % clear the command window
close all % close all fgures 
format longG % format in longG pi = 3.14159265358979

part_1(); % call part_1 function 
pi_estimation = part_2_3(); % call part_2_3 function 

function part_1() % part_1 function 
disp('Part 1 is currently running.');
start_range = 2500; % start point for the for loop
increment_range = 2500;% increment for the for loop
end_range = start_range * 100; % % end point for the for loop

time_matrix = start_range:increment_range:end_range; % initialize a time matrix that has the same length as the for loop
time_counter = 1; % time_counter starts at 1 

pi_est_matrix = start_range:increment_range:end_range;% initialize a pi_est_matrix that has the same length as the for loop

for range_counter = start_range:increment_range:end_range % for loop that uses range_counter
    tic % start the tic function
    pi_est = calc_pi(range_counter); % call calc_pi with parameters range_counter and store the pi_est matrix into pi_est
    time_matrix(time_counter) = toc; % store the time it took to run calc_pi
    pi_est_matrix(time_counter) = pi_est(range_counter); % store the calculated value of pi into pi_est_matrix at index time_counter
    time_counter = time_counter + 1; % increment the time_counter
    
end % end of function for range_counter

plot_pi_diff(range_counter,pi_est-pi); % call the plot_pi_diff to plot pi_diff vs number of points 
scatter_pi_diff_vs_time(time_matrix,pi_est_matrix-pi); % call the scatter_pi_diff_vs_time to plot the accurarcy of time vs computational time 
fprintf('\nPart 1 is finish!'); % print to command window 'Part 1 is finish!'

end % end of function part_1

function pi_est = calc_pi(range) % clac_pi function takes in a range a returns pi_est; a 1 x range matrix which contains estimated values of pi 
pi_est = zeros(1,range); % initialize the pi_est matrix
in_circle = 0; % initial counter for points in the circle
for i = 1:range % for loop from 1 to range
    if rand(1)^2 + rand(1)^2 <= 1 % if the randomly created point is in the circle
        in_circle = in_circle + 1; % increments the counter for points in the circle
    end
    pi_est(i) = (4 * in_circle/i); % pi_est(i) = calculated value of pi with the current data ; 4 * P(data point in the circle)
end % end of for loop 
end % end of calc_pi function

function plot_pi_diff(range,pi_diff_matrix) % plot_pi_diff plots the difference of pi vs as range increases 
figure('Name','Part 1a', 'Position', [0, 50, 725, 725]); % figure name: Part 1a and sets position of the figure window
plot(1:range,pi_diff_matrix); % plots a line graph of the (pi_est - pi) vs as range increases 
ylim([-0.02,0.02]); % sets the y axis 
yline(0,'r--'); % creates a horizontal red line at 0 
title(['Acurarcy of \pi vs Number of Data Points', '; Estimated value of pi: ',(num2str(pi_diff_matrix(range) + pi))]); % title of figure 
xlabel(['Number of Data Points: ', num2str(range)]); % the label for x axis
ylabel('Difference of Estimated Value of pi with pi'); % the label for y axis
end %end of function

function scatter_pi_diff_vs_time(time_matrix,pi_diff_matrix) % call the scatter_pi_diff_vs_time to plot the accurarcy of time vs computational time 
figure('Name','Part 1b', 'Position', [850, 50, 725, 725]); % figure name: Part 1a and sets position of the figure window
scatter(time_matrix,pi_diff_matrix,'.') % plot a scatter graph of accurarcy of pi vs computational time 
ylim([-0.02,0.02]); % sets the y axis 
yline(0,'r--'); % creates a horizontal red line at 0 
title('Accuracy of \pi vs Computational Time'); % title of figure 
xlabel('Computational Time (seconds)'); % the label for x axis
ylabel('Difference of Estimated Value of pi with pi'); % the label for y axis
legend([num2str(length(time_matrix)), ' Data Points']);% have a legend that dislays how many data points are used
end % end of function

function pi_calc = part_2_3()% part 2 and 3 funtion
fprintf('\n__________________________________\n'); 

sig_fig = input('Input the number of sig figures: '); %ask user to input the number of sig figs wanted to estimated value of pi
n_points = 0; % number of points on the graph 
in_circle = 0; % number of points in the circle
counter = 0; % counter to check if the SSD
pi_est_matrix = zeros(1,100^sig_fig); % a matrix that contains value of previously evaluated pi

figure('Name','Part 2 and 3', 'Position', [425, 50, 725, 725]); % figure name: Part 1; position of window
xlim([0,1]); % x coordinates
ylim([0,1]); % y coordinates
xlabel(['Number of data points: ', num2str(n_points)]); % label the x coordinates 
hold on % retains plot in the current axes so that new plots added to the axes do not delete existing plots 

while 1 % while loops that always run until it breaks 
    randX = rand(1); % create a random variable called randX
    randY = rand(1); % create a random variable called randY
    if (randX^2 + randY^2) <= 1 % if the point is less then 1, it is in the circle
        plot(randX,randY,'r.'); % plot a red point in the circle
        in_circle = in_circle + 1; % increment the in_circle counter
    else
        plot(randX,randY,'b.'); % else plot a blue point outside the circle
    end
    n_points = n_points + 1; % increment the number of points in total
    pi_est_matrix(n_points) = (4 * in_circle/n_points); % calculate the estimated value of pi with current data
    
    if (mod(n_points,10^(sig_fig)) == 0) % after every n_points mod 10^(sig_fig)
        drawnow(); % update the figure and in realtime draw the points 
        title(['Calculated pi = ', sprintf(['%.',num2str(sig_fig),'f'],pi_est_matrix(n_points)),'; Sig Fig: (', num2str(sig_fig), ')']); % title
        xlabel(['Number of data points: ', num2str(n_points)]); % xlabel
    end
    
    if (n_points > 10^(sig_fig+1)) % if the number of points(n_points) is greater than 10^(sig-fig+1), this allows the code to run for a certain amount of time for more accurate results
        SSD = std(pi_est_matrix(n_points-100:n_points)); % Calculate SSD = sample standard deviation of the last 100 estimations of pi 
        if(SSD < 0.1^(1+sig_fig)) % if the SSD is below 0.1^(1+sig_fig)
            counter = counter + 1; % increment counter 
            if counter >= 10^(sig_fig+1) % checks to see if the counter has been broken 
                xlabel(['Number of data points: ', num2str(n_points)]); % xlabel
                title(['Calculated pi = ', sprintf(['%.',num2str(sig_fig),'f'],pi_est_matrix(n_points)),'; Sig Fig: (', num2str(sig_fig), ')']); %title of figure
                pi_calc = pi_est_matrix(n_points); % calculated value of pi 
                disp(['The calculated value of pi for ', num2str(sig_fig), ' significant figures is: ', sprintf(['%.',num2str(sig_fig),'f'],pi_est_matrix(n_points))]);% print results to user
                break % break out of while loop
            end
        end
    end
    
end % end of while loop

hold off % allows creation of new plots without overiding the previous one 

end % end of part 2 and 3