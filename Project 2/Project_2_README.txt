User Guide for Project 2 AMS595 

1) Download all 3 file: Project_2_AMS595_Joseph_Chan.m , Project_2_Matrix_Input.txt , and Project_2_Matrix_Inverse.txt
2) Project_2_AMS595_Joseph_Chan.m contains the script for both Part 1 and 2. 
3) Project_2_Matrix_Input.txt is a text file that contains the matrix input to be used in the matrix_inverse function in part 2. 
    A matrix has already been saved there and can be edited.
4) Project_2_Matrix_Inverse.txt is a empty text file that will contains the inverse of the matrix input when the script is run and user request to write to the file
5) Before running the script(Project_2_AMS595_Joseph_Chan.m) decide if the matrix input shall be passed to the function as a variable or a textfile.
    5a) As a variable change the matrix_input in the editor to the desire matrix. 
    5b) As a text file changed the matrix_input in Project_2_Matrix_Input.txt to the desire matrix. 
6) Run Project_2_AMS595_Joseph_Chan.m
7) Part 1 will begin and it will create a plot that plots the Computational Time vs the size of a n by n matrix.
8) After Part 1 is finish, Part 2 wil begin. 
9) Part 2 will ask for the user to input 0 or 1; inputting anything else will ask the user to try again. 
10) The inverse of the matrix_input will be calculated using elementary matrices and multiplying them together to get the inverse.
11) Next, the Command Window will prompt the user to input 0, 1, or 2; inputting anything else will ask the user to try again.
12) Depending on the user input, the script will save the inverse to the Workspace or Project_2_Matrix_Inverse.txt or BOTH 
13) After calculating the inverse, the last part of Project 2 begins 
14) LUP decomposition will be called on the matrix_input
15) L, U, and P will be stored in the Workspace, they will also be displayed in the Command Window 
16) Project 2 is complete.