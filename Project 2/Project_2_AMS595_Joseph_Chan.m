%% Clears the workspace and command window 
clear
clc
close all
format short
%% Start of Project 1; Gaussian (Gauss-Jordan) elimination .
disp("Start of Project 1");
disp("Creating plot for Computational Time vs size of n x n matrix");
plot_G_elim();
%% Start of Project 2; Matrix inversion and LU decomposition .
fprintf("\nStart of Project 2\n");
matrix_input = load("Project_2_Matrix_Input.txt");
matrix_inverse = inv_matrix(matrix_input);
[L, U, P] = LUP_decomp(matrix_input);
fprintf("\nThe L U P decomposition of matrix input is:\n")
L
U
P
%% Project 2 Part 2.
%% part 2c. L U P decomposition
function [L, U, P] = LUP_decomp(input_matrix)
L = eye(size(input_matrix));
P = eye(size(input_matrix));
 
FE = input_matrix; % store the matrix into FE
[m,n] = size(FE); % store the dimensions of the matrix 
%while (testRowEche(FE) ~= true)
[row_i,col_j] = deal(1,1); % row_i = col_j = 1; initial position of pointer 
while (row_i <= m && col_j <= n) % makes sure the current pointer does not go out of bound 
    if(FE(row_i,col_j)~=0) % step 2: if current pointer is a no-zero pivot
        for row_k = row_i+1:1:m % step 5 loops through the rows below row_i
            L(row_k,col_j) = (FE(row_k,col_j)/FE(row_i,col_j)); % 
            FE(row_k,:) = FE(row_k,:)-(FE(row_k,col_j)/FE(row_i,col_j))*FE(row_i,:); % step 5 use the pivot to zero out the rows below it 
        end
        if(LastNonZeroRow(FE,row_i)==true) % step 6, if row_i is the last non-zero row, return FE
            FE = triu(FE); % sets the entries below the diagonal to 0 
            break % break out of the function 
        else % step 6, return back to step 2 butf increment row_i and col_j
            row_i = row_i + 1;
            col_j = col_j + 1;
        end
    else % step 3 if current pointer is a zero 
        for row_k = row_i+1:1:m % step 3: loops through the rows below row_i; Won't run if row i = m
            if(FE(row_k,col_j)~=0)% step 3: A non-zero pivot has been found 
                P([row_i,row_k],:)=P([row_k,row_i],:); % swap the two rows
                FE([row_i,row_k],:)=FE([row_k,row_i],:); % swap the two rows
                break % break out of the for loop
            end
            if row_k == m % step 4: if the entire column has no non-zero pivots, go to the next column to the right 
                col_j = col_j+1;  % increment col_j
            end
        end % will return to step 2 after this 
        if(LastNonZeroRow(FE,row_i)==true) % step 6, If pointer is at the last non_zero row, the matrix is now in echelon form 
            FE = triu(FE); % sets the entries below the diagonal to 0 
            break % break out of the function 
        end
    end
end % end of while loop
FE = triu(FE); % sets the entries below the diagonal to 0 
U = FE;

% inv(P) * L * U check
end
%% part 2b. Matrix Inverse Function
function return_matrix = inv_matrix(input_matrix)
while 1 %run until user input correct input
disp("Enter 0 to use the variable passed to the function"); 
input_int_1 = input("Enter 1 to use the variable in the text file: "); % ask for user input 
switch input_int_1
    case 0
        disp("0 has been entered")
        input_temp_matrix = input_matrix;
        
        if (is_invert(input_temp_matrix) == false) % if the matrix is not invertible, return the function 
            return
        end
        
        E = elem_matrices(input_temp_matrix); % store the elementary matrices of the input matrix to E 
        break
    case 1
        fprintf("1 has been entered\nThe default .txt input is Project_2_Matrix_Input.txt\n");
        input_string_1 = input("Enter the name of the text file, include .txt at the end: ",'s'); % store the user input string to input_string_1 
        input_temp_matrix = load(input_string_1); %E = load("Project_2_Matrix_Input.txt"); % load the file as a matrix 
        
        if (is_invert(input_temp_matrix) == false) % if the matrix is not invertible, return the function 
            return
        end
        
        E = elem_matrices(input_temp_matrix); % store the elementary matrices of the input matrix to E
        break
    otherwise
        clc
    disp("The input was neither a 0 or a 1, try again.")
end

end

fprintf("\n\nCalculating the inverse of the matrix\n\n")

% calculate the inverse of the matrix by multiplying the elementary matrimatrixes backwards order 

E_x = E{length(E)}; 
for x = length(E)-1:-1:1 
    E_x = E_x * E{x};
end
return_matrix = E_x; % store the inverse to return_matrix 
%return_matrix * input_temp_matrix

while true %run until user inputs the correct input 
disp("Enter 0 to store the inverse of the matrix to the workspace");
disp("Enter 1 to store the inverse of the matrix to a text file")
input_int_2 = input("Enter 2 for BOTH: "); %ask for user input and store as an integer
switch input_int_2
    case 0
        fprintf("\nThe inverse of the matrix has been stored in the Workspace named matrix_inverse");
        break
    case 1
        fprintf("\nThe inverse of the matrix has been stored in the text file named Project_2_Matrix_Inverse.txt");
        writematrix(return_matrix,"Project_2_Matrix_Inverse.txt") % write the matrix to a file 
        break
    case 2
        fprintf("\nThe inverse of the matrix has been stored in the Workspace and in Project_2_Matrix_Inverse.txt");
        writematrix(return_matrix,"Project_2_Matrix_Inverse.txt") % write the matrix to a file 
        break
    otherwise
        clc
        fprintf("The input was not a 0, 1, or 2. Try again.");
end
end
end % end of function 
%% Part 2a. function array_of_elem_matrices = elem_matrices(input_matrix); Find the elementary matrices of the matrix 
function array_of_elem_matrices = elem_matrices(input_matrix)
if size(input_matrix,1) ~= size(input_matrix,2) % if input matrix is not a square matrix 
    disp("the inputted matrix is not a square matrix ")
    return
end
if (det(input_matrix) <= 1e-10 && det(input_matrix) >= -1e-10) % if the def of the input matrix is 0 or close to 0 
    disp("the inputted matrix is not invertible because the determiant is equal to 0")
    return 
end
array_of_elem_matrices = {};
% forward elim
FE = input_matrix; % store the matrix into FE
I = eye(size(FE)); % identity matrix the same size as FE 
[m,n] = size(FE); % store the dimensions of the matrix 
[row_i,col_j] = deal(1,1); % row_i = col_j = 1; initial position of pointer 
while (row_i <= m && col_j <= n) % makes sure the current pointer does not go out of bound 
    if(FE(row_i,col_j)~=0) % step 2: if current pointer is a no-zero pivot
        for row_k = row_i+1:1:m % step 5 loops through the rows below row_i 
            I(row_k,:) = I(row_k,:)-(FE(row_k,col_j)/FE(row_i,col_j))*I(row_i,:); % store the multiplier to I
            array_of_elem_matrices(length(array_of_elem_matrices)+1)={I}; % store I to cell array 
            I = eye(size(FE)); % reset I to identity matrix 
            FE(row_k,:) = FE(row_k,:)-(FE(row_k,col_j)/FE(row_i,col_j))*FE(row_i,:); % step 5 use the pivot to zero out the rows below it
        end
        if(LastNonZeroRow(FE,row_i)==true) %  step 6, if row_i is the last non-zero row, return FE
            FE = triu(FE); % sets the entries below the diagonal to 0 
            break % break out of the loop 
        else % step 6, return back to step 2 butf increment row_i and col_j
            row_i = row_i + 1;
            col_j = col_j + 1;
        end
    else % step 3 if current pointer is a zero 
        for row_k = row_i+1:1:m % step 3: loops through the rows below row_i; Won't run if row i = m
            if(FE(row_k,col_j)~=0)% step 3: A non-zero pivot has been found 
                
                I([row_i,row_k],:)=I([row_k,row_i],:); % swap the two rows in I 
                array_of_elem_matrices(length(array_of_elem_matrices)+1)={I}; % store I in the Cell array
                I = eye(size(FE)); % reset I to the identity matrix
                
                FE([row_i,row_k],:)=FE([row_k,row_i],:); % swap the two rows
                break % break out of the for loop
            end
            if row_k == m % step 4: if the entire column has no non-zero pivots, go to the next column to the right 
                col_j = col_j+1;  % increment col_j
            end
        end % will return to step 2 after this 
        if(LastNonZeroRow(FE,row_i)==true) % step 6, If pointer is at the last non_zero row, the matrix is now in echelon form 
            FE = triu(FE); % sets the entries below the diagonal to 0 
            break % break out of the function 
        end
    end
end % end of while loop
FE = triu(FE); % sets the entries below the diagonal to 0 
% back sub 
BS = FE;
row_i = size(BS,1); % store the number of rows to row_i
while(row_i >= 1) % Loops until i is 1; if i is 1 then this matrix is in row-canonical form
    if(row_pivot_index(BS,row_i) == 0) % this row is all zeroes 
        row_i = row_i - 1;
        continue 
    end
    % step 1: begin with row_i as the last non_zero row
    if(row_pivot_index(BS,row_i) ~=0) % this row has a pivot 
        col_j = leadingZeroes(BS,row_i)+1; % step 1: begin with col_j as the first non-zero entry(pivot in this row 
        
        I(row_i,:) = I(row_i,:)/BS(row_i,col_j); % convert the pivot entry to "1"
        array_of_elem_matrices(length(array_of_elem_matrices)+1) = {I}; % store I to the cell array 
        I = eye(size(BS)); % reset I to the identity matrix
                
        BS(row_i,:) = BS(row_i,:)/BS(row_i,col_j); % step 2: convert the pivot entry to "1" 
        
        for row_index = row_i-1:-1:1 % step 3: loop for the entries about the pivot
            
            I(row_index,:) = I(row_index,:)-BS(row_index,col_j)*I(row_i,:); % use the pivot to zero out the corresponding entry in all rows above it 
            array_of_elem_matrices(length(array_of_elem_matrices)+1) = {I};
            I = eye(size(BS));
            
            BS(row_index,:) = BS(row_index,:)-BS(row_index,col_j)*BS(row_i,:); % step 3: use the pivot to zero out the corresponding entry in all rows above it.\
            
        end
    end
    row_i = row_i - 1; % step 4: decrement row_i and find the new pivot and go back to step 1
end % end of while loop
end
%% function return_boolean = is_invert(input_matrix); Check if a Square Matrix is invertible or non singular
function return_boolean = is_invert(input_matrix)
if size(input_matrix,1) ~= size(input_matrix,2) % if input matrix is not a square matrix 
    disp("the inputted matrix is not a square matrix ")
    return_boolean = false;
    return
end
if (det(input_matrix) <= 1e-10 && det(input_matrix) >= -1e-10) % if the def of the input matrix is 0 or close to 0 
    disp("the inputted matrix is not invertible because the determiant is equal to 0")
    return_boolean = false;
    return 
end
return_boolean = true;
end
%% Project 2 Part 1.  
%% part 1e. function plot_G_elim()
%   Create a scatter plot that test various square matrixes of size n vs computational time 
%   Create a line plot that test various square matriexes of size n vs average computational time 
function plot_G_elim()
x_axis_length = 100; % the end range of the x axis 
num_trials = 10; % number of trials per x 
x_axis_nxn = []; % a vector that will have the x axis at the end 
x_axis_nxn_avrg = 1:1:x_axis_length; % a vector that is preallocated and will be used as the x-axis for the average time, [1 2 3 4 ... x_axis_length]
x_axis_nxn_helper = zeros(1,num_trials); % a vector that will be used in the loop
time = []; % a vector that will have the y axis at the end, has all the time 
time_helper = zeros(1,num_trials); % a vector that will be used in the loop 
time_avrg = zeros(1,x_axis_length); % a vector that will have the y axis at then end and will have the avrg time 
for c1 = 1:1:x_axis_length % loop for the length of the x_axis
    for c2 = 1:1:num_trials % loop for the number of trials 
        rand_A = rand(c1); % create a random c1 x c1 matrix 
        rand_b = rand(c1,1); % create a random c1 x 1 vector 
        tic % start of tic 
        G_elim(rand_A,rand_b); % call Gaussian elimination on randA and randB
        time_helper(c2) = toc; % store toc in time_helper 
        x_axis_nxn_helper(c2) = c1; % store the value of c1 to x_axis_nxn_helper
    end
    time = [time, time_helper]; % use the helper function to fill out the time vector 
    x_axis_nxn = [x_axis_nxn, x_axis_nxn_helper]; % use the helper function to fill out the x_axis_nxn vector 
    time_avrg(c1) = mean(time_helper); % store the average time to time_avrg
end

figure('Name','Part 1e', 'Position', [25, 50, 1500, 725]); % figure name: Part 1e and sets position of the figure window
hold on % hold on the figure 
title("Computational Time(seconds) vs Size of an n x n matrix"); % title 
xlim([0,x_axis_length+1]) % x axis 
xlabel("Size of an n x n matrix") % x axis label 
ylim([0,0.04]) % y axis 
ylabel("Computational Time (seconds)") % y axis label 
scatter(x_axis_nxn,time,"."); % create a scatter plot with x_axis_nxn vs time 
plot(x_axis_nxn_avrg,time_avrg,"-ro","LineWidth",1,'MarkerEdgeColor','k', 'MarkerFaceColor', 'r','MarkerSize',4); % create a line plot with x_axis_nxn_avrg vs time_avrg
legend("10 data points",'Average data points') % create a legend 
hold off % hold off the figure 
end % end of function
%% part 1d. function x = G_elim(A,b);Gaussian (Gauss-Jordan) Elimination
%   Solve a system of linear equations by using Gaussian (Gauss-Jordan) Elimniation 
%   A: coefficient matrix; b: constant 
%   x: solution vector 
function x = G_elim(A,b) 
if (size(A,1) ~= size(b,1)) % the number of rows in A should equal the number of rows in B  
    x = [];
    disp("Dimensions of A and b being are not consistent to use Gaussian (Gauss-Jordan) Elimination.");
    return
end
if (rank(A)~= rank([A,b])) % if the rank of A does not equal the rank of Ab
    x = [];
    disp("The rank of A is not equal to the rank of the augmented matrix A*b, then there is no unique solutions"); %Rouché–Capelli theorem 
    return
end
Ab = [A b]; % augmented matrix Ab
Ab = back_sub(forward_elim(Ab)); % first use forward_elim to get Ab in row echelon form, then use back_sub to put Ab in row-Canoical form
x = Ab(:,end); % the solution vector is at the last column of the augmented matrix A*b
 
end % end of function
%% part 1c. function BS = back_sub(input_matrix); Backward Substitution
%   Use Backward Substiution to convert a matrix in echelon form into row-canonical form 
%   input_matrix: matrix to be inputted
%   BS: matrix that is now in row-canonical form 
function BS = back_sub(input_matrix)
BS = input_matrix; % store the matrix into BS

if(testRowEche(BS)==false) % if input matrix is not in row echelon form 
    disp("return an appropriate error if the input matrix is not in echelon form");
    return % break out of the function 
end

row_i = size(BS,1); % store the number of rows to row_i
while(row_i >= 1) % Loops until i is 1; if i is 1 then this matrix is in row-canonical form
    if(row_pivot_index(BS,row_i) == 0) % this row is all zeroes 
        row_i = row_i - 1;
        continue 
    end
    % step 1: begin with row_i as the last non_zero row
    if(row_pivot_index(BS,row_i) ~=0) % this row has a pivot 
        col_j = leadingZeroes(BS,row_i)+1; % step 1: begin with col_j as the first non-zero entry(pivot in this row 
        BS(row_i,:) = BS(row_i,:)/BS(row_i,col_j); % step 2: convert the pivot entry to "1" 
        for row_index = row_i-1:-1:1 % step 3: loop for the entries about the pivot
            BS(row_index,:) = BS(row_index,:)-BS(row_index,col_j)*BS(row_i,:); % step 3: use the pivot to zero out the corresponding entry in all rows above it. 
        end
    end
    row_i = row_i - 1; % step 4: decrement row_i and find the new pivot and go back to step 1
end % end of while loop
end % end of function
%% part 1b. function FE = forward_elim(input_matrix); Forward Elimination; 
%   Coverts a matrix to echelon form 
%   input_matrix: matrix to be inputted 
%   FE: returns the matrix but in echelon form 
function FE = forward_elim(input_matrix)
FE = input_matrix; % store the matrix into FE
[m,n] = size(FE); % store the dimensions of the matrix 
[row_i,col_j] = deal(1,1); % row_i = col_j = 1; initial position of pointer 
while (row_i <= m && col_j <= n) % makes sure the current pointer does not go out of bound 
    if(FE(row_i,col_j)~=0) % step 2: if current pointer is a no-zero pivot
        for row_k = row_i+1:1:m % step 5 loops through the rows below row_i
            FE(row_k,:) = FE(row_k,:)-(FE(row_k,col_j)/FE(row_i,col_j))*FE(row_i,:); % step 5 use the pivot to zero out the rows below it 
        end
        if(LastNonZeroRow(FE,row_i)==true) % step 6, if row_i is the last non-zero row, return FE
            FE = triu(FE); % sets the entries below the diagonal to 0 
            return % break out of the function 
        else % step 6, return back to step 2 butf increment row_i and col_j
            row_i = row_i + 1;
            col_j = col_j + 1;
        end
    else % step 3 if current pointer is a zero 
        for row_k = row_i+1:1:m % step 3: loops through the rows below row_i; Won't run if row i = m
            if(FE(row_k,col_j)~=0)% step 3: A non-zero pivot has been found 
                FE([row_i,row_k],:)=FE([row_k,row_i],:); % swap the two rows
                break % break out of the for loop
            end
            if row_k == m % step 4: if the entire column has no non-zero pivots, go to the next column to the right 
                col_j = col_j+1;  % increment col_j
            end
        end % will return to step 2 after this 
        if(LastNonZeroRow(FE,row_i)==true) % step 6, If pointer is at the last non_zero row, the matrix is now in echelon form 
            FE = triu(FE); % sets the entries below the diagonal to 0 
            return % break out of the function 
        end
    end
end % end of while loop
FE = triu(FE); % sets the entries below the diagonal to 0 
end % end of function
%% function return_logic = LastNonZeroRow(input_matrix,input_row);Last non-zero row 
%   checks to see if the row inputted is the last-non zero row in the matrix 
%   input_matrix: matrix to be inputted; input_row: row to be checked
%   return_logic: true if the input row is the last non-zero row, ow false 
function return_logic = LastNonZeroRow(input_matrix,input_row)
M = input_matrix;
m = size(M,1);
for row_k = m:-1:1 % start at the bottom
    if(row_pivot_index(M,row_k)~=0) % find a non-zero entry in the row; row_k = last non_zero row  
        if(row_k == input_row) % if row_k = input_row, then input_row is the last non_zero row input_matrix
            %row_k
            return_logic = true;
            return
        else
            %row_k
            return_logic = false;
            return
        end
    end
end
end
%% Part 1a. return_int = test_form_1a(input_matrix); Test which form the matrix is in 
%   test_form checks if a matrix is in echelon form, a row canonical form, or neither
%   Arguments - input_matrix: matrix that will be tested on 
%   Returns - return_int: returns return_int which is equal to a integer; 0 = neither; 1 = row echelon; 2 = row canoical 
function return_int = test_form_1a(input_matrix) 
if testRowEche(input_matrix) == true % check if the matrix is in Row Echelon form
    if testRowCano(input_matrix,true) == true  % check if the matrix is in Row Canoical form
        return_int = 2; % set return_int = 2
        disp("Row Canoical") % display to command window "Row Canoical"
    else % else, the row is not in Row Canoical form BUT is in Row Echelon
        return_int = 1; % set return_int = 1 
        disp("Row Echelon") % display to command window "Row Echelon"
    end
else % if the matrix is not in Row Echelon form
    return_int = 0; % set return_int = 0 
    disp("Neither") % display to command window "Neither"
end
end % end of function 
%% return_logic = testRowCano(input_matrix); Test is the matrix is in Row Canonical form
%   function that checks if a matrix is in row canoical form, returns true if it is, false if it is not 
%   Arguments - input_matrix: matrix that will be tested on; in_echelon: check if the matrix is in echelon form to begin with 
%   Returns - return_logic: returns true if in Row Canoical form; 0 = false; 1 = true 
function return_logic = testRowCano(input_matrix,in_echelon)
return_logic = true; % set return_logic = true 
if(in_echelon == true) % if the matrix is in row echelon form 
    for current_row = 1:1:size(input_matrix,1) % loop for the number of rows in input_matrix
        current_row_pivot = row_pivot_index(input_matrix,current_row); % find the pivot in the current row 
        if current_row_pivot >= 1 % the pivot is in this current row at col index current_row_pivot
            if current_row > 1 % the pivot is the first row has already been checked, now we check the entries above the pivot
                if all_zeroes_above(input_matrix,current_row,current_row_pivot) == false % if the entries above the current pivot are not all 0 
                    return_logic = false; % set return_logic to false 
                    return % exit the function 
                end
            end
        elseif current_row_pivot == -1% the first non-zero entry is not a 1 
            return_logic = false; % set return_logic to false 
            return % exit the function 
        else
            % do nothing
        end
    end
else
    return_logic = false; % if the matrix is not in row echelon form 
end
end % end of function 
%% return_logic = testRowEche(input_matrix); Test is the matrix is in Row Echelon Form 
%   function checks if the inputted matrix is in Row Echelon Form
%   Arguments - input_matrix: matrix that is inputted
%   Returns - return_logic: returns true if in Row Echelon form; 0 = false; 1 = true 
function return_logic = testRowEche(input_matrix)
return_logic = true; % initialize return_logic to be true 
for matrix_row = 1:1:size(input_matrix,1)-1 % loop for the number of rows in the matrix - 1; - 1 so next_row_zeroes will not go out of bound 
current_row_zeroes = leadingZeroes(input_matrix,matrix_row); % set the number of leading zeroes in the current row 
next_row_zeroes = leadingZeroes(input_matrix,matrix_row+1); % set the number of the leader zeroes in the next row (row below current) 
if current_row_zeroes >= next_row_zeroes % if the leading zeroes of current row >= leading zeroes of the next row; means the row below it has less or equal amount of leading zeroes
    if current_row_zeroes > next_row_zeroes % if the leading zeroes of current row > leading zeroes of the next row; 
        return_logic = false; % set return_logic to false 
        %disp('Not Row Echelon non leading zero'); % display 'Not Row Echelon non leading zero'
        %disp(matrix_row); % display which row that broke this conditon     
        break 
    elseif (current_row_zeroes == next_row_zeroes) && (current_row_zeroes ~= size(input_matrix,2)) % checks to see if the row is not a row of all zeroes    
        return_logic = false; % set return_logic to false 
        %disp('Not Row Echelon there is a non zero entry above'); % display 'Not Row Echelon there is a non zero entry above'
        %disp(matrix_row); % display which row that broke this conditon   
        break
    else % else do nothing 
    end
end
end % end of for loop
if return_logic == true % if return_logic is still true 
    %disp('Row Echelon') % display 'Row Echelon' 
end
end
%% function return_int = leadingZeroes(input_matrix,input_row); Returns the number of leading zeroes a row has 
%   Function that returns the number of leading zeroes a row has before the first non-zero entry 
%   Arguments - input_matrix: matrix that is inputted; input_row: row that will be taken from the matrix 
%   Returns - return_int: the number of leading zeroes the row has 
function return_int = leadingZeroes(input_matrix,input_row)
matrix_row = input_matrix(input_row,:); % store the row of the input matrix into matrix_row 
row_length = length(matrix_row);% store the length of the matrix_row into row_length

for row_index = 1:1:row_length % loops for the length of the matrix_row
    if matrix_row(row_index) ~= 0 % if current index of the matrix_row is a non-zero number 
        return_int = row_index - 1; % set return_int to row_index - 1: number of leading zeroes in the row 
        return % break out of the for loop and function  
    end
end

if row_index == row_length % if the index is at the end of the row 
    if matrix_row(row_index) == 0 % if the value of the index at the end of the row is a zero 
        return_int = row_length; % set return_int to row_length: this row has all zeroes 
    end
end
end % end of funtion 
%% return_int = row_pivot_index(input_matrix,input_row); Return the index of the first pivot in a row
%   Function that returns the index of the pivot of the row.
%   Arguments - input_matrix: inputted matrix; input_row: row of the matrix 
%   Returns- return int = -1 if the first non-zero entry is not a 1 
%            return int =  0 if there the entire row is just 0s 
%            return int >= 1 if the fist non-zero entry is a 1 or if it not the only entry in the row it returns that index(pivot)
function return_int = row_pivot_index(input_matrix,input_row)
matrix_row = input_matrix(input_row,:); % store the row of the input matrix into matrix_row 
row_length = length(matrix_row);% store the length of the matrix_row into row_length
for row_index = 1:1:row_length % loops for the length of the matrix_row
    if matrix_row(row_index) ~= 0 % if current index of the matrix_row is a non-zero number 
        if matrix_row(row_index) ~= 1 % if the current index of the matrix is not a 1 (not a pivot)
            return_int = -1; % set return_int to - 1
            return % break out of the function
        else % if the current index of the matrix is a 1 and a pivot
            return_int = row_index; %set return_int to row_index
            return
        end
    end
end

if row_index == row_length % if the index is at the end of the row 
    if matrix_row(row_index) == 0 % if the value of the index at the end of the row is a zero 
        return_int = 0; % set return_int to row_length; this row has all zeroes 
    end
end
end % end of function
%% return_logic = all_zeroes_above(input_matrix,input_row,input_col); Check if the entries above the pivot are all zeroes 
% Function that returns true if the entries above the pivot are all zeroes 
% Arguments - input_matrix: inputted matrix; input_row: row of the matrix 
% Returns - return_logic = 1 = true if the entries above the pivot are all zeroes 
%           return_logic = 0 = false if the entries above the pivot are not all zeroes 
function return_logic = all_zeroes_above(input_matrix,input_row,input_col)
return_logic = true; % initialize return_logic 
if input_row == 1 % there are no zeroes above the entry in the first row 
    return_logic = true; 
    return
else 
    for row_above_pivot = input_row-1:-1:1 % loop for entries above the pivot
        if input_matrix(row_above_pivot,input_col)~=0 % looks for an entry that is non-zero
            return_logic = false; % return false if a non-zero entry is found
            return
        end
    end
end
end% end of function
%% End of Project 2